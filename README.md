## Create React App Visualization

Read more about this assessment here

// EOG assignment hugo rodriguez

Libraries used:
- Recharts@1.4.2 for chart visualization
- React-google-maps@9.4.5 for map visualization

First I had to look at some Redux Saga tutorials because I haven't use it

After this I looked at the folder's structure and tried to figure out 
how Redux Saga was implemented along with the reducers and the api folder provided

Then I tried to replicate the way findWeatherById and findLocationByLang fetch 
the information from their provided api. This way was how I obtained the drone 
metric information by fetching to the API provided at the assignment site.
After this, I did the same for the DroneMets reducer and the 
DroneMets saga. Tried to do it the way was already implemented.

Once this was done I created a Main component to be rendered instead
of the NowWhat component inside the App component. this is connected to store, to gather
the metrics of the drone every 4 seconds after initial render and also
pass this information to MyChart component. Main and also contains MyMap component

MyChart component was developed with recharts, it is also connected to the store 
and uses the data passed on the props from Main component to display it in a LineChart. the way to use
it is to pass the props.data to LineChart and use the data keys to asign it to the
different axis, in this case, keys are 'temperature' for Y axis and 'time' for X axis.
Also I provided some formater Functions to not display the raw data in the tooltip, axes and labels.

MyMap Component is pretty much the same the Get Started example of the library and it is connected
to the Redux Store to get the drone's lat and long that are used to display yhe marker

I have commented some lines of all these file I've created