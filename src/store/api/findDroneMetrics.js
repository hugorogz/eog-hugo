import "isomorphic-fetch";

// here we fetch the drone metrics, to the provided API
// it is pretty much the same logic found at findLocationByLatLng and findWeatherById

const findDroneMetrics = async () => {
    // use asyn await as in the other members of API 
    // to store the response with drone metric data
    const response = await fetch("https://react-assessment-api.herokuapp.com/api/drone");

    if (!response.ok) {
        return { error: { code: response.status } };
    }
    const json = await response.json();
    return json;
};

export default findDroneMetrics;
