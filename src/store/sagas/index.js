import WeatherSagas from "./Weather";
import MetricsSagas from "./DroneMets";
import ApiErrors from "./ApiErrors";

export default [...ApiErrors, ...WeatherSagas, ...MetricsSagas];// added here metrics sagas and spread it
