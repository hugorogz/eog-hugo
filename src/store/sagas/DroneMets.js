import { takeEvery, call, put, cancel, all } from "redux-saga/effects";
import API from "../api";
import * as actions from "../actions";

// here i am fetching for the metrics of the drone by using API.findDroneMetrics....
// this does it in a similar way that Weather.js here as sagas Does


function* watchFetchDrone() {
    const { error, data: metrics } = yield call(API.findDroneMetrics);// call the findDroneMetrics@api function
    if (error) { // error handling
        console.log({ error });
        yield put({ type: actions.API_ERROR, code: error.code });
        yield cancel();
        return;
    }
    if (!metrics.length) {
        yield put({ type: actions.API_ERROR });
        yield cancel();
        return;
    }
    // "put(someAction)"" creates an Effect description that instructs saga to dispatch an action to the Store.
    yield put({ type: actions.DRONE_METRICS_RECEIVED, metrics })
}

function* watchMetricsReceived(action){
    const { metrics } = action;
    const {latitude, longitude} = metrics[metrics.length -1];
    // "put(someAction)"" creates an Effect description that instructs saga to dispatch an action to the Store.
    yield put({type: actions.FETCH_WEATHER, latitude, longitude});

}

function* watchAppLoad() {
    // all([...effects])
    // Creates an Effect description that instructs the middleware to run multiple "effects" 
    // in parallel and wait for all of them to complete. 
    // It's quite the corresponding API to standard Promise#all.
    yield all([
        // takeEvery(pattern, saga, ...args)
        // this spawns a "saga" on each action dispatched to the Store that matches "pattern".
        takeEvery(actions.FETCH_METRICS, watchFetchDrone),
        takeEvery(actions.DRONE_METRICS_RECEIVED, watchMetricsReceived),
    ]);
}


export default [watchAppLoad];
