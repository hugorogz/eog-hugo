import * as actions from "../actions";

// i have done this part very similar to the Weather.js@reducers

const initialState = {// set the state for the metrics
    loading: false,
    metrics: [],
    latitude: null,
    longitude: null,
}

const startLoading = (state, action) => {
    return { ...state, loading: true };
};

const droneMetricsReceived = (state, action) => {// this metrics came from Metrics@sagas

    const { metrics } = action;

    if (!metrics.length) return state;

    return {
        ...state,
        loading: false,
        metrics,
    }

};

const latlongReceived = (state, action) => {

    const { latitude, longitude } = action;
    return {
        ...state,
        latitude,
        longitude,
    }

};

const handlers = {
    // these [actions.SOMETHING] come from Metric@Sagas
    [actions.FETCH_METRICS]: startLoading,
    [actions.DRONE_METRICS_RECEIVED]: droneMetricsReceived,
    [actions.FETCH_WEATHER]: latlongReceived,

};

export default (state = initialState, action) => {

    const handler = handlers[action.type];

    if (typeof handler === "undefined") return state;
    
    return handler(state, action);

};
