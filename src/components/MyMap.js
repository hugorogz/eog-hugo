import React from "react"
import { compose, withProps } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";// google maps library for react
import { connect } from 'react-redux';

// use mapStateToProps to map the Redux State at store
// to the components props
const mapStateToProps = (state) => ({
    latitude: state.metrics.latitude,
    longitude: state.metrics.longitude,
});

// connect our map component to store, with some defined props for the map
const Map = connect(mapStateToProps)(compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?&key=AIzaSyC-FGexpt6GXffZ-W10l7pFNPI6xU0axsM",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `350px`, width: `50%` }} />,
        mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
)((props) => (
    <GoogleMap // defining GoogleMap component with some attributes
        defaultZoom={5}
        defaultCenter={{ lat: props.latitude, lng: props.longitude}}
        center={{ lat: +props.latitude, lng: +props.longitude}}
    >
        {props.isMarkerShown && <Marker position={{ lat: +props.latitude, lng: +props.longitude }} />}
    </GoogleMap>)
))

// MyMap Componentt, this is meant to display MyMapComponent we
// have connected to the store
class MyMap extends React.PureComponent {
    render() {
        return (
            <Map
                isMarkerShown={true}
            />
        )
    }
}



export default MyMap;