// import all we need including components, action and redux connect

import React from 'react';
import { connect } from 'react-redux';
import MyMap from './MyMap';
import MyChart from './MyChart';
import * as actions from '../store/actions';

// use mapStateToProps to map the Redux State at store
// to the components props
const mapStateToProps = (state) => ({
    metrics: state.metrics.metrics,
});

// use mapStateToDispatch to map the Redux Actions at store
// to the components props and be able to dispatch actions form inside the component
const mapStateToDispatch = (dispatch) => ({
    findDroneMetrics: () => dispatch({ type: actions.FETCH_METRICS }),
});

// Main component

// 
class Main extends React.Component {
    
    componentDidMount() {
        //after first render get the metrid data from the drone
        this.props.findDroneMetrics();
        this.intervalId = setInterval(() => {// set an interval every 4 seconds to gather the metric data again
            this.props.findDroneMetrics();
        }, 4000);

    }

    render() {

        const { metrics } = this.props;

        const chartData = metrics.map(objMetric => {// get data for the chart
            return ({
                time: objMetric.timestamp,
                temperature: objMetric.metric,
            })
        }
        );
        
        return (
            <div>
                <h2>Map Visualization</h2>
                <MyMap />

                <h2>Chart Visualization</h2>
                <MyChart data={chartData} />
            </div>
        );
    }
}

export default connect(mapStateToProps, mapStateToDispatch)(Main);
