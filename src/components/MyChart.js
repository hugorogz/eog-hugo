import React from 'react';
// recharts library for handling the graph
import { CartesianGrid, LineChart, Line, Label, ResponsiveContainer, Tooltip, XAxis, YAxis} from 'recharts';
import { connect } from 'react-redux';

// array of months for displaying the date in a mor understandablke way (no numbers)
const months = 
["January", "February", "March", "April", "May", "June", "July", "August", "Sepember", "October", "November", "December"];

//  get the date
const getTimes = (last) => {
    
    if (!last) {
        return;
    }

    const date = new Date(last.time);
    const day = date.getDay();
    const month = date.getMonth();
    
    return `${months[month]} ${day}`;
}

const yAxisNumbers = (number) => Math.round(number);// number formatter for chart

const xAxisDates = (time) => {// dates formatter for chart@ X Axis

    const date = new Date(time);
    const minutes = date.getMinutes();
    const hours = date.getHours();

    if (minutes % 5 !== 0) {// to only display time in multiples of 5 
        return '';
    }

    return `${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}`;

}

const tooltipDates = (time) => {// tooltip date format

    const date = new Date(time);
    const minutes = date.getMinutes();
    const hours = date.getHours();
    const seconds = date.getSeconds();
    const day = date.getDay();
    const month = date.getMonth();
    const year = date.getFullYear();

    return `${months[month]} ${day}, ${year}, 
    ${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}:${seconds}` ;

}

const tooltipFormat = (value, name, props) => `${value.toFixed(4)}`;// Formatter for the tooltip

// create the chart inside a ResponsiveContainer
// ResponsiveContainer helps to make charts adapt to the size of parent container. 
// One of the props width and height should be a percentage string.

// we define the chart to be a LineChart, wich displays data in a Line Way (other are AreaChart or BarChart)
 
// CartesianGrid for showing a background grid
// LineChartrecieves the data from the props
// Line just defines the tiupe of line we are going to display
// some options like the color, and if we want to display just the line or also dots

const MyChart = props => ( 
   
    <ResponsiveContainer width="50%" height={350}>
       
        <LineChart data={props.data} margin={{ top: 10, bottom: 30}}>

            <CartesianGrid strokeDasharray="3 3" />
            
            <Line type="linear" dataKey="temperature" stroke="#0224CE" dot={false} />
            
            <XAxis dataKey="time" tickFormatter={xAxisDates} >

                <Label position="bottom" >
                    {getTimes(props.data[props.data.length - 1])}
                </Label>

            </XAxis>

            <YAxis dataKey="temperature" type="number" domain={['dataMin', 'dataMax']} tickFormatter={yAxisNumbers} />
            
            <Tooltip formatter={tooltipFormat} labelFormatter={tooltipDates}/>

        </LineChart>

    </ResponsiveContainer>
    
)

export default connect()(MyChart);// connect the chart component to redux store